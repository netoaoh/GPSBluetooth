﻿using Foundation;
using UIKit;

namespace GPSBluetooth.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{

			var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
			UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);


			global::Xamarin.Forms.Forms.Init();
			Xamarin.FormsMaps.Init();

			if (options != null)
			{
				if (options.ContainsKey(UIApplication.LaunchOptionsLocalNotificationKey))
				{
					var localNotification = options[UIApplication.LaunchOptionsLocalNotificationKey] as UILocalNotification;
					if (localNotification != null)
					{
						UIAlertController okayAlertController = UIAlertController.Create(localNotification.AlertAction, localNotification.AlertBody, UIAlertControllerStyle.Alert);
						okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

						Window.RootViewController.PresentViewController(okayAlertController, true, null);

						UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
					}
				}
			}


			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}
