﻿using System;
using GPSBluetooth.Models;
using GPSBluetooth.Models.Entities;
using GPSBluetooth.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(GPSBluetooth.iOS.Services.AppService))]
namespace GPSBluetooth.iOS.Services
{
	public class AppService : IAppService
	{
		public AppService()
		{
		}

		public bool Save(Message msg)
		{
			using (var db = new ModelOcorrencia())
			{
				var data = new Ocorrencia
				{
					DataOcorrencia = msg.DataHoraAtual,
					TipoOcorrencia = msg.Occurrence,
					Descricao = msg.Description,
					Latitude = msg.Latitude,
					Longitude = msg.Longitude
				};

				Ocorrencia check = db.ListarPorOcorrencia(data);

				if (check == null)
				{
					db.Insert(data);
					return true;
				}
			}

			return false;
		}
	}
}
