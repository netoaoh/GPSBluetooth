﻿using System;
using System.Collections.Generic;
using Foundation;
using GPSBluetooth.Services;
using MultipeerConnectivity;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;
using GPSBluetooth.Models;
using GPSBluetooth.Models.Entities;

namespace GPSBluetooth.iOS
{
	public class MPCHandler : MCSessionDelegate
	{
		public MCPeerID PeerID { get; set; }
		public MCSession Session { get; set; }
		public MCNearbyServiceBrowser Browser { get; set; }
		//public MCAdvertiserAssistant Advertiser { get; set; }
		public MCNearbyServiceAdvertiser Advertiser { get; set; }

		private IBluetoothService _bluetoothMessage;

		public List<MCPeerID> ConectedDevides { get; set; }

		public MPCHandler()
		{
			_bluetoothMessage = DependencyService.Get<IBluetoothService>();
			ConectedDevides = new List<MCPeerID>();
		}

		public void SetupPeerWithDisplayName(string displayName)
		{
			PeerID = new MCPeerID(displayName);

		}

		public void SetupSession()
		{
			Session = new MCSession(PeerID);
			Session.Delegate = this;

		}

		public void SetupBrowser()
		{
			Browser = new MCNearbyServiceBrowser(PeerID, "testService");
			Browser.Delegate = new BrowserHandler(Session, this);
		}

		public void SetupAdvertise()
		{
			Advertiser = new MCNearbyServiceAdvertiser(PeerID, new NSDictionary(), "testService");
			Advertiser.StartAdvertisingPeer();
			Advertiser.Delegate = new AdvertiserHandler(Session);
		}


		public override void DidChangeState(MCSession session, MCPeerID peerID, MCSessionState state)
		{
			switch (state)
			{
				case MCSessionState.Connected:
					ConectedDevides.Add(peerID);

					Message msg = new Message
					{
						Occurrence = Message.REQUEST_MESSAGE,
						DataHoraAtual = DateTime.Now
					};

					_bluetoothMessage.SendMessage(msg, true, false);
					break;
				case MCSessionState.Connecting:
					break;
				case MCSessionState.NotConnected:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public override void DidFinishReceivingResource(MCSession session, string resourceName, MCPeerID fromPeer, NSUrl localUrl, NSError error)
		{
			
		}

		public override void DidReceiveData(MCSession session, NSData data, MCPeerID peerID)
		{
			var json = JObject.Parse(data.ToString());

			if (json.SelectToken("Occurrence").ToString() == Message.REQUEST_MESSAGE)
			{
				using (var db = new ModelOcorrencia())
				{
					List<Ocorrencia> tempData = db.ListarTodos();

					foreach (var oc in tempData)
					{
						Message msg = new Message
						{
							Occurrence = oc.TipoOcorrencia,
							Description = oc.Descricao,
							Latitude = oc.Latitude,
							Longitude = oc.Longitude,
							DataHoraAtual = oc.DataOcorrencia
						};

						_bluetoothMessage.SendMessage(msg, false, false);
					}
				}
			}
			else
			{

				Message msg = new Message
				{
					Occurrence = json.SelectToken("Occurrence").ToString(),
					Description = json.SelectToken("Description").ToString(),
					Latitude = Convert.ToDouble(json.SelectToken("Latitude").ToString()),
					Longitude = Convert.ToDouble(json.SelectToken("Longitude").ToString()),
					DataHoraAtual = Convert.ToDateTime(json.SelectToken("DataHoraAtual").ToString())
				};

				_bluetoothMessage.ReadMessage(msg);
			}
		}

		public override void DidReceiveStream(MCSession session, NSInputStream stream, string streamName, MCPeerID peerID)
		{
			
		}

		public override void DidStartReceivingResource(MCSession session, string resourceName, MCPeerID fromPeer, NSProgress progress)
		{
			
		}
	}
}
