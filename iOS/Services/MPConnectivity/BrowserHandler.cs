﻿using System;
using Foundation;
using MultipeerConnectivity;

namespace GPSBluetooth.iOS
{
	public class BrowserHandler : MCNearbyServiceBrowserDelegate
	{
		NSData context = null;
		MCSession Session { get; set; }
		MPCHandler Mpc { get; set; }

		public BrowserHandler(MCSession session, MPCHandler mpc)
		{
			Session = session;
			Mpc = mpc;
		}

		public override void FoundPeer(MCNearbyServiceBrowser browser, MCPeerID peerID, NSDictionary info)
		{
			browser.InvitePeer(peerID, Session, context, 60);

		}

		public override void LostPeer(MCNearbyServiceBrowser browser, MCPeerID peerID)
		{
			
		}

		public override void DidNotStartBrowsingForPeers(MCNearbyServiceBrowser browser, NSError error)
		{
			
		}
	}
}
