﻿using System;
using Foundation;
using MultipeerConnectivity;
using ObjCRuntime;

namespace GPSBluetooth.iOS
{
	public class AdvertiserHandler : MCNearbyServiceAdvertiserDelegate
	{
		MCSession Session { get; set; }

		public AdvertiserHandler(MCSession session)
		{
			Session = session;
		}

		public override void DidReceiveInvitationFromPeer(MCNearbyServiceAdvertiser advertiser, MCPeerID peerID, NSData context, MCNearbyServiceAdvertiserInvitationHandler invitationHandler)
		{
			invitationHandler(true, Session);
		}
	}
}
