﻿using System;
using System.IO;
using Xamarin.Forms;
using SQLite.Net.Interop;
using SQLite.Net.Platform.XamarinIOS;
using GPSBluetooth.Services;

[assembly: Dependency(typeof(GPSBluetooth.iOS.Services.Config))]
namespace GPSBluetooth.iOS.Services
{
	public class Config : IConfig
	{
		private string dataBaseName = "GPSBluetooth.db3";
		private string dataBaseDir = "";
		private ISQLitePlatform plataform = null;


		public Config()
		{
		}

		public string DataBaseName
		{
			get
			{
				return dataBaseName;
			}
		}

		public string DataBaseDir
		{
			get
			{
				if (String.IsNullOrWhiteSpace(dataBaseDir))
				{
					var personal = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					dataBaseDir = Path.Combine(personal, "..", "Library");
				}

				return dataBaseDir;
			}
		}

		public ISQLitePlatform Plataform
		{
			get
			{
				if (plataform == null)
				{
					plataform = new SQLitePlatformIOS();
				}

				return plataform;
			}
		}
	}
}
