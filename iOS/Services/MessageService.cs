﻿using System;
using System.Threading.Tasks;
using GPSBluetooth.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(GPSBluetooth.iOS.Services.MessageService))]
namespace GPSBluetooth.iOS.Services
{
	public class MessageService : IMessageService
	{
		public MessageService()
		{
		}

		public async Task DisplayAlert(string title, string message, string buttonText)
		{
			await App.Current.MainPage.DisplayAlert(title, message, buttonText);
		}

		public void Notify(Message msg)
		{
			Device.BeginInvokeOnMainThread(() => {
				App.Current.MainPage.DisplayAlert(msg.Occurrence, msg.Description, "OK");
			});
		}
	}
}
