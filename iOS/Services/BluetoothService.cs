﻿using System;
using System.Threading.Tasks;
using Foundation;
using GPSBluetooth.Services;
using MultipeerConnectivity;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(GPSBluetooth.iOS.Services.BluetoothService))]
namespace GPSBluetooth.iOS.Services
{
	public class BluetoothService : IBluetoothService
	{
		MPCHandler mpcHandler;
		private IMessageService _messageService;
		private IAppService _appService;

		public BluetoothService()
		{
			this._messageService = DependencyService.Get<IMessageService>();
			this._appService = DependencyService.Get<IAppService>();
		}

		public void Setup()
		{
			mpcHandler = new MPCHandler();
			mpcHandler.SetupPeerWithDisplayName(UIDevice.CurrentDevice.Name);
			mpcHandler.SetupSession();
			mpcHandler.SetupBrowser();
			mpcHandler.SetupAdvertise();
		}

		public void StartScanning()
		{
			mpcHandler.Browser.StartBrowsingForPeers();
		}

		public void StopScanning()
		{
			mpcHandler.Browser.StopBrowsingForPeers();
		}

		public void SendMessage(Message msg, bool request, bool save)
		{
			NSError error = null;
			var peers = mpcHandler.Session.ConnectedPeers;

			var formattedMsg = "{\"Sender\": \"" + UIDevice.CurrentDevice.Name + "\", \"Occurrence\": \"" + msg.Occurrence + "\", \"Description\": \"" + msg.Description + "\", \"Latitude\": \"" + msg.Latitude + "\", \"Longitude\": \"" + msg.Longitude + "\", \"DataHoraAtual\": \"" + msg.DataHoraAtual + "\"}";

			if (request)
			{
				mpcHandler.Session.SendData(NSData.FromString(formattedMsg), peers, MCSessionSendDataMode.Reliable, out error);
			}
			else
			{
				if (save)
				{
					if (_appService.Save(msg))
					{
						mpcHandler.Session.SendData(NSData.FromString(formattedMsg), peers, MCSessionSendDataMode.Reliable, out error);
					}
				}
				else
				{
					mpcHandler.Session.SendData(NSData.FromString(formattedMsg), peers, MCSessionSendDataMode.Reliable, out error);
				}
			}

			Console.WriteLine(error);
		}

		public void ReadMessage(Message msg)
		{
			if (_appService.Save(msg))
			{
				_messageService.Notify(msg);
				SendMessage(msg, false, false);
			}
		}
	}
}
