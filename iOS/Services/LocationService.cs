﻿using System;
using System.Threading.Tasks;
using CoreLocation;
using GPSBluetooth.Services;
using PerpetualEngine.Location;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

[assembly: Dependency(typeof(GPSBluetooth.iOS.Services.LocationService))]
namespace GPSBluetooth.iOS.Services
{
	public class LocationService : ILocationService
	{
		LocationManager locationManager;
		public Position pos;

		public LocationService()
		{
			locationManager = new LocationManager();
			locationManager.StartLocationUpdates();

			locationManager.LocationUpdated += HandleLocationChanged;
		}

		public void HandleLocationChanged(object sender, LocationUpdatedEventArgs e)
		{
			CLLocation location = e.Location;
			pos = new Position(location.Coordinate.Latitude, location.Coordinate.Longitude);
		}

		public Position GetCurrentLocation()
		{
			return pos;
		}
	}
}
