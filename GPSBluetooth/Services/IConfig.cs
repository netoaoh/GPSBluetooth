﻿using System;
using SQLite.Net.Interop;

namespace GPSBluetooth.Services
{
	public interface IConfig
	{
		string DataBaseName { get; }
		string DataBaseDir { get; }
		ISQLitePlatform Plataform { get; }
	}
}
