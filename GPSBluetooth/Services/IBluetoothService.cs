﻿namespace GPSBluetooth.Services
{
	public interface IBluetoothService
	{
		void Setup();
		void StartScanning();
		void StopScanning();
		void SendMessage(Message msg, bool request, bool save);
		void ReadMessage(Message msg);
	}
}
