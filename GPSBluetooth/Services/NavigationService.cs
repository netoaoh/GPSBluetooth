﻿using System;
using System.Threading.Tasks;
using GPSBluetooth.Views;

namespace GPSBluetooth.Services
{
	public class NavigationService : INavigationService
	{
		public NavigationService()
		{
		}

		public async Task Back()
		{
			await App.Current.MainPage.Navigation.PopAsync(true);
		}

		public async Task NavigateTo(string page, object param)
		{
			switch (page)
			{
				case "Main":
					await App.Current.MainPage.Navigation.PushAsync(new GPSBluetoothPage());
					break;
				case "Notificar":
					await App.Current.MainPage.Navigation.PushAsync(new NotificarView(param));
					break;
				case "Notificacoes":
					await App.Current.MainPage.Navigation.PushAsync(new NotificacoesView());
					break;
			}
		}
	}
}
