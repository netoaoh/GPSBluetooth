﻿using System;
using System.Threading.Tasks;

namespace GPSBluetooth.Services
{
	public interface INavigationService
	{
		Task NavigateTo(string page, object param);
		Task Back();
	}
}
