﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace GPSBluetooth.Services
{
	public interface ILocationService
	{
		Position GetCurrentLocation();
	}
}