﻿using System;
using System.Threading.Tasks;

namespace GPSBluetooth.Services
{
	public interface IMessageService
	{
		Task DisplayAlert(string tilte, string message, string buttonText);
	    void Notify(Message msg);
	}
}
