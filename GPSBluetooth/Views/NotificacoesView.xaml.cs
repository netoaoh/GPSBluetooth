﻿using System;
using System.Collections.Generic;
using GPSBluetooth.Models;
using GPSBluetooth.Models.Entities;
using GPSBluetooth.ViewModels;
using Xamarin.Forms;

namespace GPSBluetooth.Views
{
	public partial class NotificacoesView : ContentPage
	{
		public NotificacoesView()
		{
			InitializeComponent();
			this.BindingContext = new NotificacoesViewModel();
		}
	}
}
