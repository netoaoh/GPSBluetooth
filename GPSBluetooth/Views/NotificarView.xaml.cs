﻿using System;
using System.Collections.Generic;
using GPSBluetooth.Models.Entities;
using GPSBluetooth.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace GPSBluetooth.Views
{
	public partial class NotificarView : ContentPage
	{
		public NotificarView(object param)
		{
			InitializeComponent();
			this.BindingContext = new NotificarViewModel();

			((NotificarViewModel)this.BindingContext).OcurrenceLocation = (Position)param;

			Dictionary<string, string> tipos = ((NotificarViewModel)this.BindingContext).GetTipoOcorrencia();

			TipoOcorrencia.SelectedIndexChanged += (sender, args) =>
			{
				((NotificarViewModel)this.BindingContext).OcurrenceType = TipoOcorrencia.Items[TipoOcorrencia.SelectedIndex];
			};

			foreach (string tipo in tipos.Keys)
			{
				TipoOcorrencia.Items.Add(tipo);
			}
		}

		void LimparClicked(object sender, EventArgs e)
		{
			Editor editor = this.FindByName<Editor>("Notificacao");

			editor.Text = "";
		}
	}
}
