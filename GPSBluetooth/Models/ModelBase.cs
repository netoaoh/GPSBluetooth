﻿using System;
using Xamarin.Forms;
using SQLite.Net;
using System.IO;
using GPSBluetooth.Services;

namespace GPSBluetooth.Models
{
	/// <summary>
	/// Classe que serve de base para todas as models
	/// </summary>
	public class ModelBase<T> : IDisposable
	{
		/// <summary>
		/// Objeto de conexão SQLite
		/// </summary>
		protected SQLiteConnection _conexao;

		/// <summary>
		/// Construtor da classe ModelBase
		/// </summary>
		public ModelBase()
		{
			var config = DependencyService.Get<IConfig>();
			_conexao = new SQLiteConnection(config.Plataform, Path.Combine(config.DataBaseDir, config.DataBaseName));

			CreateTable();
		}

		/// <summary>
		/// Cria a tabela no banco de dados
		/// </summary>
		public void CreateTable()
		{
			_conexao.CreateTable<T>();
		}

		/// <summary>
		/// Deleta a tabela do banco de dados
		/// </summary>
		public void DropTable()
		{
			_conexao.DropTable<T>();
		}

		/// <summary>
		/// Metodo que insere um registro na tabela
		/// </summary>
		/// <param name="data">Data.</param>
		public void Insert(T data)
		{
			_conexao.Insert(data);
		}

		/// <summary>
		/// Metodo que atualiza um registro da tabela
		/// </summary>
		/// <param name="data">Data.</param>
		public void Update(T data)
		{
			_conexao.Update(data);
		}

		/// <summary>
		/// Metodo que exclui um registro da tabela
		/// </summary>
		/// <param name="data">Data.</param>
		public void Delete(T data)
		{
			_conexao.Delete(data);
		}

		#region Implementação IDisposable

		/// <summary>
		/// Releases all resource used by the <see cref="T:OutubroRosa.ModelBase`1"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="T:OutubroRosa.ModelBase`1"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="T:OutubroRosa.ModelBase`1"/> in an unusable state. After
		/// calling <see cref="Dispose"/>, you must release all references to the <see cref="T:OutubroRosa.ModelBase`1"/> so
		/// the garbage collector can reclaim the memory that the <see cref="T:OutubroRosa.ModelBase`1"/> was occupying.</remarks>
		public void Dispose()
		{
			_conexao.Close();
		}

		#endregion
	}
}
