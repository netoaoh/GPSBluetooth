﻿using System;
using System.Collections.Generic;
using GPSBluetooth.Models.Entities;

namespace GPSBluetooth.Models
{
	public class ModelTipoOcorrencia : ModelBase<TipoOcorrencia>
	{
		public ModelTipoOcorrencia()
			: base()
		{
			DropTable();
			CreateTable();
			Insert(new TipoOcorrencia
			{
				Ocorrencia = "Engarrafamento"
			});

			Insert(new TipoOcorrencia
			{
				Ocorrencia = "Acidente"
			});

			Insert(new TipoOcorrencia
			{
				Ocorrencia = "Alagamento"
			});
		}

		public List<TipoOcorrencia> ListarTodos()
		{
			string query = "SELECT * FROM TipoOcorrencia";
			return _conexao.Query<TipoOcorrencia>(query);
		}
	}
}
