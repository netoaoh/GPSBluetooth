﻿using System;
using System.Collections.Generic;
using GPSBluetooth.Models.Entities;

namespace GPSBluetooth.Models
{
	public class ModelOcorrencia : ModelBase<Ocorrencia>
	{
		public ModelOcorrencia()
			:base()
		{
		}

		public List<Ocorrencia> ListarTodos()
		{
			string query = "SELECT DISTINCT DataOcorrencia, TipoOcorrencia, Latitude, Longitude, Descricao FROM Ocorrencia";
			return _conexao.Query<Ocorrencia>(query);
		}

		public List<Ocorrencia> ListarOcorrencias()
		{
			string query = "SELECT DISTINCT TipoOcorrencia, Descricao FROM Ocorrencia";
			return _conexao.Query<Ocorrencia>(query);
		}

		public Ocorrencia ListarPorOcorrencia(Ocorrencia other)
		{
			string query = "SELECT * FROM Ocorrencia WHERE TipoOcorrencia = ? AND Latitude = ? AND Longitude = ?";
			List<Ocorrencia> l = _conexao.Query<Ocorrencia>(query, other.TipoOcorrencia, other.Latitude, other.Longitude);

			if (l.Count > 0)
				return l[0];
			else
				return null;
		}
	}
}
