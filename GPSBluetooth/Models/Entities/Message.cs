﻿using System;
namespace GPSBluetooth
{
	public class Message
	{
		public const string REQUEST_MESSAGE = "Request";

		public string Occurrence { get; set;}
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public string Description { get; set; }
		public DateTime DataHoraAtual { get; set; }

		public Message()
		{
		}
	}
}
