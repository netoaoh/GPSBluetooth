﻿using System;
using SQLite.Net.Attributes;

namespace GPSBluetooth.Models.Entities
{
	public class TipoOcorrencia
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		public string Ocorrencia { get; set; }

		public TipoOcorrencia()
		{
		}
	}
}
