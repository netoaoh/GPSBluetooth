﻿using System;
using SQLite.Net.Attributes;

namespace GPSBluetooth.Models.Entities
{
	public class Ocorrencia
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		public DateTime DataOcorrencia { get; set; }
		public string TipoOcorrencia { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public string Descricao { get; set; }

		public Ocorrencia()
		{
		}
	}
}
