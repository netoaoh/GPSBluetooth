﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BluetoothLE.Core;
using GPSBluetooth.Models;
using GPSBluetooth.Models.Entities;
using GPSBluetooth.Services;
using GPSBluetooth.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace GPSBluetooth
{
	public partial class GPSBluetoothPage : ContentPage
	{
		private ILocationService location;

		IBluetoothService service;

		Position pos;

		public GPSBluetoothPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			this.BindingContext = new MainViewModel();

			location = DependencyService.Get<ILocationService>();
			service = DependencyService.Get<IBluetoothService>();

			MapViewer.Tap += (sender, e) =>
			{
				((MainViewModel)this.BindingContext).NavNotificar(e.Position);
			};

			UpdateMap();

			service.Setup();
			service.StartScanning();
		}

		private void UpdateMap()
		{
			Device.StartTimer(TimeSpan.FromSeconds(5), () =>
			{
				Task.Factory.StartNew(() =>
					{
						pos = location.GetCurrentLocation();
						Device.BeginInvokeOnMainThread(() =>
						{
							MapViewer.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMiles(0.5)));

							using (var db = new ModelOcorrencia())
							{
								List<Ocorrencia> data = db.ListarTodos();

								foreach (var oc in data)
								{
									if (DateTime.Now.Subtract(oc.DataOcorrencia).Minutes > 30)
									{
										db.Delete(oc);
									}
									else
									{
										var newPin = new Pin
										{
											Type = PinType.Place,
											Position = new Position(oc.Latitude, oc.Longitude),
											Label = oc.TipoOcorrencia,
											Address = oc.Descricao
										};

										MapViewer.Pins.Add(newPin);
									}
								}
							}

							UpdateMap();
						});
					});

				return false;
			});
		}

		public void SliderChange(object sender, ValueChangedEventArgs e)
		{
			var zoomLevel = e.NewValue;
			var latlongdegrees = 360 / (Math.Pow(2, zoomLevel));
			MapViewer.MoveToRegion(new MapSpan(MapViewer.VisibleRegion.Center, latlongdegrees, latlongdegrees));
		}

		void HandleClicked(object sender, EventArgs e)
		{
			var b = sender as Button;
			switch (b.Text)
			{
				case "Rua":
					MapViewer.MapType = MapType.Street;
					break;
				case "Hibrido":
					MapViewer.MapType = MapType.Hybrid;
					break;
				case "Satelite":
					MapViewer.MapType = MapType.Satellite;
					break;
			}
		}
	}
}
