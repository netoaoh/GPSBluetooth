﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using GPSBluetooth.Models;
using GPSBluetooth.Models.Entities;
using GPSBluetooth.Services;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace GPSBluetooth.ViewModels
{
	public class NotificarViewModel : BaseViewModel
	{
		private readonly INavigationService _navigationService;
		private readonly IMessageService _messageService;
		private readonly IBluetoothService _bluetoothService;

		private string _notificationDescription;
		public string NotificationDescription
		{
			get { return _notificationDescription; }
			set
			{
				_notificationDescription = value;
				this.Notify("Notificacao");
			}
		}

		private Position _ocurrenceLocation;
		public Position OcurrenceLocation
		{
			get { return _ocurrenceLocation; }
			set
			{
				_ocurrenceLocation = value;
				this.Notify("OcurrenceLocation");
			}
		}

		private string _ocurrenceType;
		public string OcurrenceType
		{
			get { return _ocurrenceType; }
			set
			{
				_ocurrenceType = value;
				this.Notify("OcurrenceType");
			}
		}

		public ICommand NotificarCommand { get; set; }
		public ICommand NavNotificarCommand { get; set; }
		public ICommand NavNotificacoesCommand { get; set; }

		public NotificarViewModel()
		{
			this._navigationService = DependencyService.Get<INavigationService>();
			this._messageService = DependencyService.Get<IMessageService>();
			this._bluetoothService = DependencyService.Get<IBluetoothService>();

			this.NotificarCommand = new Command(this.Notificar);
			this.NavNotificarCommand = new Command(this.NavNotificar);
			this.NavNotificacoesCommand = new Command(this.NavNotificacoes);
		}

		private void Notificar()
		{
			if (OcurrenceLocation != null)
			{
				var msg = new Message
				{
					DataHoraAtual = DateTime.Now,
					Occurrence = OcurrenceType,
					Latitude = OcurrenceLocation.Latitude,
					Longitude = OcurrenceLocation.Longitude,
					Description = NotificationDescription
				};

				this._bluetoothService.SendMessage(msg, false, true);
				this._navigationService.Back();
			}
			else
			{
				_messageService.DisplayAlert("Aviso", "Informe uma descrição", "Ok");
			}
		}

		public Dictionary<string, string> GetTipoOcorrencia()
		{
			Dictionary<string, string> ret = new Dictionary<string, string>();
			using (var db = new ModelTipoOcorrencia())
			{
				List<TipoOcorrencia> oc = db.ListarTodos();

				foreach (TipoOcorrencia tipo in oc)
				{
					ret.Add(tipo.Ocorrencia, tipo.Ocorrencia);
				}
			}

			return ret;
		}

		public async void NavNotificar()
		{
			await this._navigationService.NavigateTo("Notificar", null);
		}

		public async void NavNotificacoes()
		{
			await this._navigationService.NavigateTo("Notificacoes", null);
		}
	}
}
