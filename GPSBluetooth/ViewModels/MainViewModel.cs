﻿using System;
using System.Windows.Input;
using GPSBluetooth.Services;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace GPSBluetooth.ViewModels
{
	public class MainViewModel : BaseViewModel
	{
		private readonly INavigationService _navigationService;

		public ICommand NavNotificarCommand { get; set; }
		public ICommand NavNotificacoesCommand { get; set; }

		public MainViewModel()
		{
			this._navigationService = DependencyService.Get<INavigationService>();
			this.NavNotificacoesCommand = new Command(this.NavNotificacoes);
		}

		public async void NavNotificar(Position pos)
		{
			await this._navigationService.NavigateTo("Notificar", pos);
		}

		public async void NavNotificacoes()
		{
			await this._navigationService.NavigateTo("Notificacoes", null);
		}
	}
}
