﻿using System;
using System.Collections.Generic;
using GPSBluetooth.Models;
using GPSBluetooth.Models.Entities;

namespace GPSBluetooth.ViewModels
{
	public class NotificacoesViewModel : BaseViewModel
	{
		public List<Ocorrencia> _notificacoes;
		public List<Ocorrencia> Notificacoes
		{
			get { return _notificacoes; }
			set
			{
				_notificacoes = value;
				this.Notify("Notificacoes");
			}
		}

		public string Resumo { get { return " Existem " + Notificacoes.Count + " ocorrências reportadas."; } }

		public NotificacoesViewModel()
		{
			using (var db = new ModelOcorrencia())
			{
				Notificacoes = db.ListarOcorrencias();
			}

		}
	}
}
